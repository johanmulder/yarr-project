/**
 * 
 * File created at 3 apr. 2013 22:28:01 by Johan Mulder <johan@mulder.net>
 */
package yarr.app;

import java.net.UnknownHostException;

import javax.naming.AuthenticationException;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import yarr.feed.fetch.FeedFetcher;
import yarr.feed.fetch.FeedFetcherImpl;
import yarr.stor.dao.MongoDbUtil;
import yarr.stor.dao.impl.MongoDbUtilImpl;

/**
 * Singleton for various object containers.
 * @author Johan Mulder <johan@mulder.net>
 */
public class ObjectContainer
{
	private static final Logger logger = LoggerFactory.getLogger(ObjectContainer.class);
	private static ObjectContainer instance = null;
	private yarr.stor.dao.DAOFacade feedDAOFacade = null;
	private FeedFetcher fetcher = null;
	private MongoDbUtil mongoDbUtil = null;
	private Scheduler jobScheduler = null;
	
	/**
	 * Private constructor.
	 * @throws AuthenticationException
	 * @throws UnknownHostException
	 * @throws SchedulerException 
	 */
	private ObjectContainer() throws AuthenticationException, UnknownHostException, SchedulerException
	{
		// TODO: Make this configurable.
//		mongoDbUtil = new MongoDbUtilImpl("dev.local", "yarr");
		mongoDbUtil = new MongoDbUtilImpl();
		feedDAOFacade = new yarr.stor.dao.impl.DAOFacadeImpl(mongoDbUtil);
		fetcher = new FeedFetcherImpl();
		jobScheduler = StdSchedulerFactory.getDefaultScheduler();
	}
	
	/**
	 * Get _the_ instance of the ObjectContainer. This is a singleton.
	 * @return
	 */
	public static ObjectContainer getInstance()
	{
		if (instance == null)
			try
			{
				instance = new ObjectContainer();
			}
			catch (Exception e)
			{
				logger.error("ObjectContainer initialization failed", e);
				throw new RuntimeException("ObjectContainer initialization failed", e);
			}
		return instance;
	}
	
	/**
	 * Get the feed DAO facade. This facade handles all NoSQL feed DAO objects.
	 * @return
	 */
	public yarr.stor.dao.DAOFacade getFeedDAOFacade()
	{
		return feedDAOFacade;
	}
	
	/**
	 * Get the feed fetcher object.
	 * @return
	 */
	public FeedFetcher getFeedFetcher()
	{
		return fetcher;
	}
	
	/**
	 * Get the mongodb utility object.
	 * @return
	 */
	public MongoDbUtil getMongoDbUtil()
	{
		return mongoDbUtil;
	}
	
	/**
	 * Get the job scheduler.
	 * @return
	 */
	public Scheduler getJobScheduler()
	{
		return jobScheduler;
	}
}
