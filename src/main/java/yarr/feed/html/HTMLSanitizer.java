package yarr.feed.html;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Whitelist;

/**
 * Sanitize HTML for display in our feed application.
 * @author Johan Mulder <johan@mulder.net>
 */
public class HTMLSanitizer
{
	/**
	 * Sanitize a html string to strip out tags and attributes which might
	 * mess up our own formatting.
	 * @param input
	 * @return
	 */
	public String sanitize(String input, String baseURI)
	{
		Document.OutputSettings outputSettings = new Document.OutputSettings();
		outputSettings.prettyPrint(false);
		return Jsoup.clean(input, baseURI, getWhitelist(), outputSettings);
	}
	
	/**
	 * Create a whitelist for allowed html tags and attributes.
	 * @return
	 */
	private Whitelist getWhitelist()
	{
		// TODO: This should actually be stored somewhere in a configuration file.
		Whitelist whitelist = new Whitelist();
		whitelist.addAttributes("p", "dir");
		whitelist.addAttributes("a", "href");
		whitelist.addAttributes("div", "style");
		whitelist.addAttributes("img", "src", "height", "width", "alt");
		// Allow these tags.
		whitelist.addTags("b", "i", "br", "hr", "em");
		whitelist.addTags("h1", "h2", "h3", "h4");
		whitelist.addTags("ul", "ol", "li");
		whitelist.addTags("sub", "sup");
		// Enforcements
		whitelist.addEnforcedAttribute("a", "rel", "nofollow");
		// Protocols
		whitelist.addProtocols("a", "href", "http", "https");
		whitelist.addProtocols("img", "src", "http", "https");
		
		return whitelist;
	}
}
