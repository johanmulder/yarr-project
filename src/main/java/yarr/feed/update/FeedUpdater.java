/**
 * 
 * File created at 5 apr. 2013 21:56:02 by Johan Mulder <johan@mulder.net>
 */
package yarr.feed.update;

import java.util.Date;

import yarr.stor.dao.FeedDAO;
import yarr.stor.domain.Feed;

import com.sun.syndication.feed.synd.SyndFeed;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class FeedUpdater
{
	// DAO objects.
	private final FeedDAO feedDAO;
	
	public FeedUpdater(FeedDAO feedDAO)
	{
		this.feedDAO = feedDAO;
	}
	
	public void updateFromSyndFeed(Feed feed, SyndFeed feedData)
	{
		// Update these items.
		feed.setPublishDate(feedData.getPublishedDate());
		feed.setLastUpdated(new Date());
		feed.setLanguage(feed.getLanguage());
		feed.setTitle(feedData.getTitle());
		
		// Update fetch info.
		// XXX: This is not the right place to update the fetch info.
		Feed.FetchInfo fetchInfo = feed.getFetchInfo();
		if (fetchInfo == null)
		{
			fetchInfo = new Feed.FetchInfo();
			feed.setFetchInfo(fetchInfo);
		}
		fetchInfo.setLastFetched(new Date());
		fetchInfo.setLastResult("OK");
		fetchInfo.setStatus(0);
		
		
		feedDAO.update(feed);
	}

}
