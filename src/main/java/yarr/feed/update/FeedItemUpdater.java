/**
 * 
 * File created at 5 apr. 2013 20:59:44 by Johan Mulder <johan@mulder.net>
 */
package yarr.feed.update;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import yarr.stor.dao.FeedItemDAO;
import yarr.stor.domain.Feed;
import yarr.stor.domain.FeedItem;

import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class FeedItemUpdater
{
	private final FeedItemDAO feedItemDAO;
	private static final Logger logger = LoggerFactory.getLogger(FeedItemUpdater.class);
	
	/**
	 * Class constructor.
	 * @param daoFacade
	 */
	public FeedItemUpdater(FeedItemDAO feedItemDAO)
	{
		this.feedItemDAO = feedItemDAO;
	}
	
	/**
	 * Update feed items for a given feed, from given feed data.
	 * @param feed
	 * @throws FeedException 
	 * @throws IOException 
	 * @throws IllegalArgumentException 
	 */
	public void updateFromSyndFeed(Feed feed, SyndFeed feedData)
	{
		@SuppressWarnings("unchecked")
		List<SyndEntry> fetchedEntries = (List<SyndEntry>) feedData.getEntries();
		// Get all current entries from a feed. We only need to fetch the amount of entries available in the
		// new entries list, because older (already stored) entries cannot be compared as they got dropped from the
		// feed data.
		// XXX: comment above seems wrong. Fix this later on; for now fetch all items for the feed.
		Set<FeedItem> currentFeedItems = feedItemDAO.getFeedItemsForFeed(feed);
		// Get the new entries which aren't here locally yet.
		List<SyndEntry> newEntries = getNewEntries(currentFeedItems, fetchedEntries);
		// Store the new entries for the feed if there are any.
		if (newEntries.size() > 0)
			addNewEntries(feed, newEntries);
	}
	
	/**
	 * Get a list of SyndEntry objects which are not available in our own data store.
	 * @param feedItems
	 * @return The items which weren't there yet.
	 */
	private List<SyndEntry> getNewEntries(Set<FeedItem> feedItems, List<SyndEntry> fetchedEntries)
	{
		List<SyndEntry> newEntries = new ArrayList<SyndEntry>();
		for (SyndEntry entry : fetchedEntries)
			if (!syndEntryInFeedItems(entry, feedItems))
				newEntries.add(entry);
		return newEntries;
	}
	
	/**
	 * Check if a SyndEntry is already in a set of feed items.
	 * @param entry
	 * @param feedItems
	 * @return
	 */
	private boolean syndEntryInFeedItems(SyndEntry entry, Set<FeedItem> feedItems)
	{
		for (FeedItem feedItem : feedItems)
			// If the link matches, it's already been added.
			// TODO: This may not be the right check. What if a link was added twice with different contents?
			if (entry.getLink() != null && feedItem.getUrl().toExternalForm().equals(entry.getLink()))
				return true;
		return false;
	}

	/**
	 * Add new feed items to a feed.
	 * @param feed
	 * @param newEntries
	 */
	private void addNewEntries(Feed feed, List<SyndEntry> newEntries)
	{
		for (SyndEntry entry : newEntries)
		{
			try
			{
				logger.info("Adding entry {} to feed {}", entry, feed.getId());
				FeedItem newItem = FeedInfoMapper.syndEntryToFeedItem(entry);
				if (newItem != null)
				{
					// Set the feed id in the new item.
					newItem.setFeedId(feed.getId());
					// Create it.
					feedItemDAO.create(newItem);
				}
			}
			catch (MalformedURLException e)
			{
				// Log and skip the entry.
				logger.error("Invalid URL in feed item.", e);
			}
		}
	}
}
