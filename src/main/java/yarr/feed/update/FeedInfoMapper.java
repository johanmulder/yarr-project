/**
 * 
 * File created at 5 apr. 2013 21:19:45 by Johan Mulder <johan@mulder.net>
 */
package yarr.feed.update;

import java.net.MalformedURLException;
import java.util.List;

import yarr.feed.html.HTMLSanitizer;
import yarr.stor.domain.Feed;
import yarr.stor.domain.FeedItem;
import yarr.stor.domain.FeedItem.Author;

import com.sun.syndication.feed.synd.SyndContent;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class FeedInfoMapper
{
	/**
	 * Convert a SyndEntry object to a FeedItem object.
	 * @param entry
	 * @return
	 * @throws MalformedURLException 
	 */
	public static FeedItem syndEntryToFeedItem(SyndEntry entry) throws MalformedURLException
	{
		FeedItem feedItem = new FeedItem();
		feedItem.setAuthor(new Author(entry.getAuthor(), null));
		feedItem.setDescription(getDescriptionFromEntry(entry));
		feedItem.setLastUpdated(entry.getUpdatedDate());
		feedItem.setPublishDate(entry.getPublishedDate());
		feedItem.setTitle(entry.getTitle());
		feedItem.setUrl(entry.getLink());
		// https://rometools.jira.com/wiki/display/ROME/Rome+v0.4,+Feed+and+Entry+URI+Mapping
		feedItem.setGuid(entry.getUri());
		
		return feedItem;
	}
	
	public static Feed syndFeedToFeed(SyndFeed syndFeed) throws MalformedURLException
	{
		Feed feed = new Feed();
		feed.setUrl(syndFeed.getLink());
//		feed.setType(FeedType.valueOf(syndFeed.getFeedType()));
		feed.setDescription(syndFeed.getDescription());
		feed.setLanguage(syndFeed.getLanguage());
		feed.setPublishDate(syndFeed.getPublishedDate());
		feed.setTitle(syndFeed.getTitle());
		return feed;
	}
	
	/**
	 * Get the description from a SyndEntry.
	 * @param entry
	 * @return
	 */
	private static String getDescriptionFromEntry(SyndEntry entry)
	{
		@SuppressWarnings("unchecked")
		List<SyndContent> contents = entry.getContents();
		String description = null;
		if (contents != null && contents.size() > 0)
			description = contents.get(0).getValue();
		else
			description = entry.getDescription().getValue();
		// Sanitize html if it's html anyway.
		HTMLSanitizer sanitizer = new HTMLSanitizer();
		return sanitizer.sanitize(description, "/");
	}
}
