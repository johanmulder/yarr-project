/**
 * Created 2 apr. 2013 22:59:31
 */
package yarr.feed.fetch;

import java.io.IOException;
import java.net.URL;

import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;

/**
 * Feed fetcher implementation.
 * @author Johan Mulder <johan@mulder.net>
 */
public class FeedFetcherImpl implements FeedFetcher
{
	/* (non-Javadoc)
	 * @see yarr.feed.fetch.FeedFetcher#fetch(java.net.URL)
	 */
	@Override
	public SyndFeed fetch(URL url) throws IOException, IllegalArgumentException, FeedException
	{
		XmlReader reader = null;
		try
		{
	        SyndFeedInput input = new SyndFeedInput();
	        reader = new XmlReader(url);
	        return input.build(reader);
		}
		finally
		{
			if (reader != null)
				reader.close();
		}
	}
}
