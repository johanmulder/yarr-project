/**
 * 
 * File created at 8 apr. 2013 22:31:21 by Johan Mulder <johan@mulder.net>
 */
package yarr.feed.fetch;

import java.net.URL;
import java.util.Set;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import yarr.app.ObjectContainer;
import yarr.feed.update.FeedItemUpdater;
import yarr.feed.update.FeedUpdater;
import yarr.stor.dao.FeedDAO;
import yarr.stor.dao.FeedItemDAO;
import yarr.stor.domain.Feed;

import com.sun.syndication.feed.synd.SyndFeed;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class FetchJob implements Job
{
	private static final Logger logger = LoggerFactory.getLogger(FetchJob.class);

	/**
	 * 
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException
	{
		// TODO: There is too much logic in this method. Make this a service call.
		logger.info("Updating feeds");
		ObjectContainer oc = ObjectContainer.getInstance();
		FeedDAO feedDAO = oc.getFeedDAOFacade().getFeedDAO();
		FeedItemDAO feedItemDAO = oc.getFeedDAOFacade().getFeedItemDAO();
		FeedFetcher fetcher = oc.getFeedFetcher();
		FeedUpdater feedUpdater = new FeedUpdater(feedDAO);
		FeedItemUpdater feedItemUpdater = new FeedItemUpdater(feedItemDAO);
		Set<Feed> overdue = feedDAO.getOverdueFeeds();
		for (Feed feed : overdue)
		{
			try
			{
				URL feedURL = feed.getFeedUrl();
				if (feedURL == null)
				{
					logger.error("Feed {} doesn't have a valid feed url", feed.getId());
					continue;
				}
				logger.info("Feed {} ({}) is overdue", feed.getId(), feedURL.toExternalForm());
				SyndFeed newFeed = fetcher.fetch(feedURL);
				feedUpdater.updateFromSyndFeed(feed, newFeed);
				feedItemUpdater.updateFromSyndFeed(feed, newFeed);
			}
			catch (Exception e)
			{
				logger.error("Exception occurred while attempting to update feed {}", feed.getId(), e);
			}
		}
	}
}
