/**
 * 
 * File created at 3 apr. 2013 21:43:07 by Johan Mulder <johan@mulder.net>
 */
package yarr.feed.fetch;

import java.io.IOException;
import java.net.URL;

import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;

/**
 * FeedFetcher interface.
 * @author Johan Mulder <johan@mulder.net>
 */
public interface FeedFetcher
{
	/**
	 * Fetch a feed by URL.
	 * @param url
	 * @return
	 * @throws IOException
	 * @throws IllegalArgumentException
	 * @throws FeedException
	 */
	public SyndFeed fetch(URL url) throws IOException, IllegalArgumentException, FeedException;
}