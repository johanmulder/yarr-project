package yarr.stor.dao.impl;

import java.net.MalformedURLException;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Pattern;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import yarr.stor.dao.FeedItemDAO;
import yarr.stor.dao.MongoDbUtil;
import yarr.stor.domain.Feed;
import yarr.stor.domain.FeedItem;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

public class FeedItemDAOImpl implements FeedItemDAO
{
	// Logging object.
	private static final Logger logger = LoggerFactory.getLogger(FeedItemDAOImpl.class);
	// Feed item collection name.
	public static final String FEED_ITEM_COLLECTION = "feed_item";
	// DB collection for the feed item
	private DBCollection feedItemCollection = null;
	private final MongoDbUtil mongoDbUtil;
	
	/**
	 * Class constructor.
	 * @param mongoDbUtil
	 */
	public FeedItemDAOImpl(MongoDbUtil mongoDbUtil)
	{
		this.mongoDbUtil = mongoDbUtil;
	}
	
	private static interface ItemMatch
	{
		public boolean itemMatches(FeedItem item);
	}
	
	/**
	 * Get the feed item collection.
	 * @return
	 */
	private DBCollection getItemCollection()
	{
		if (feedItemCollection == null)
			feedItemCollection = mongoDbUtil.getCollection(FEED_ITEM_COLLECTION);
		return feedItemCollection;
	}

	/**
	 * Get a DBobject by id.
	 * @param id
	 * @return
	 */
	private DBObject getDataObjectById(String id)
	{
		return getItemCollection().findOne(new ObjectId(id));
	}


	@Override
	public FeedItem getById(String id)
	{
		try
		{
			return DataConverter.dataToItem(getDataObjectById(id));
		}
		catch (MalformedURLException e)
		{
			return null;
		}
	}

	@Override
	public Set<FeedItem> getFeedItemsForFeed(Feed feed)
	{
		return getFeedItemsForFeed(feed.getId(), 0);
	}

	@Override
	public Set<FeedItem> getFeedItemsForFeed(Feed feed, int limit)
	{
		return getFeedItemsForFeed(feed.getId(), limit);
	}
	

	/*
	 * (non-Javadoc)
	 * @see yarr.stor.dao.FeedItemDAO#getFeedItemsForFeed(yarr.stor.domain.Feed, int, int)
	 */
	@Override
	public Set<FeedItem> getFeedItemsForFeed(Feed feed, int limit, int offset)
	{
		// TODO Auto-generated method stub
		return getFeedItemsForFeed(feed.getId(), limit, offset);
	}


	@Override
	public Set<FeedItem> getFeedItemsForFeed(String feedId)
	{
		return getFeedItemsForFeed(feedId, 0);
	}
	
	@Override
	public Set<FeedItem> getFeedItemsForFeed(String feedId, int limit)
	{
		return getFeedItemsForFeed(feedId, limit, 0);
	}
	
	/**
	 * Get a Set of items from a cursor.
	 * @param cursor The cursor to fetch the data from.
	 * @param limit Optional limit (<= 0 for unlimited)
	 * @param offset Optional offset (0 for no offset)
	 * @param itemMatch Optional match object to verify if the item should be added to the Set.
	 * @return null if no entries or the Set of items if matches were found.
	 */
	private Set<FeedItem> getDataFromCursor(DBCursor cursor, int limit, int offset, ItemMatch itemMatch)
	{
		// Check if the cursor has additional data.
		if (!cursor.hasNext())
			return null;
		
		// Create a treeset which will be ordered by time descending.
		// TODO: This should be configurable.
		// XXX: This currently doesn't make sense, as the sorting is done by the cursor.
		Set<FeedItem> items = new TreeSet<FeedItem>(
			new FeedItemComparator(FeedItemComparator.OrderField.TIME, FeedItemComparator.OrderDirection.DESCENDING));

		// Skip the first <offset> items if requested.
		if (offset > 0)
			cursor.skip(offset);
		
		DBObject object = null;
		int added = 0;
		try
		{
			while (cursor.hasNext() && (limit <= 0 || ++added <= limit))
			{
				object = cursor.next();
				FeedItem item = DataConverter.dataToItem(object);
				if (itemMatch == null || itemMatch.itemMatches(item) )
					items.add(item);
			}
		}
		catch (MalformedURLException e)
		{
			// This can't possibly be happening, because the create method in this class should
			// already have run into this.
			if (object != null)
				logger.error("Invalid URL found in object {}: {}", object.get("_id"), object.get("url"));
		}
		finally
		{
			// Always close the cursor.
			cursor.close();
		}
		
		if (items.size() > 0)
			return items;
		return null;
	}
	
	/* (non-Javadoc)
	 * @see yarr.stor.dao.FeedItemDAO#getFeedItemsForFeed(yarr.stor.domain.Feed, int, int)
	 */
	@Override
	public Set<FeedItem> getFeedItemsForFeed(String feedId, int limit, int offset)
	{
		// Find all items with the given feed id.
		DBCursor cursor = getItemCollection().find(new BasicDBObject("feed_id", new ObjectId(feedId)));
		// Sort by publish date descending
		cursor.sort(new BasicDBObject("pubdate", -1));
		
		return getDataFromCursor(cursor, limit, offset, null);
	}


	@Override
	public void create(FeedItem feedItem)
	{
		DBObject dbobject = DataConverter.itemToData(feedItem);
		/* WriteResult result = */ getItemCollection().save(dbobject);
		ObjectId objectId = (ObjectId) dbobject.get("_id");
		feedItem.setId(objectId.toString());
	}

	@Override
	public void update(FeedItem feedItem)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(FeedItem feedItem)
	{
		delete(feedItem.getId());
	}

	@Override
	public void delete(String id)
	{
		DBObject object = getDataObjectById(id);
		getItemCollection().remove(object);
		
	}
	
	/* (non-Javadoc)
	 * @see yarr.stor.dao.FeedItemDAO#countNumFeedItems(yarr.stor.domain.Feed)
	 */
	@Override
	public int countNumFeedItems(Feed feed)
	{
		DBCursor cursor = getItemCollection().find(new BasicDBObject("feed_id", new ObjectId(feed.getId())));
		if (cursor != null)
			return cursor.count();
		return -1;
	}

	/* (non-Javadoc)
	 * @see yarr.stor.dao.FeedItemDAO#findInDescription(java.lang.String)
	 */
	@Override
	public Set<FeedItem> findInDescription(String regexp)
	{
		return findInDescription(regexp, 0, 0, null);
	}

	/* (non-Javadoc)
	 * @see yarr.stor.dao.FeedItemDAO#findInDescription(java.lang.String, int)
	 */
	@Override
	public Set<FeedItem> findInDescription(String regexp, int limit)
	{
		return findInDescription(regexp, limit, 0, null);
	}

	/* (non-Javadoc)
	 * @see yarr.stor.dao.FeedItemDAO#findInDescription(java.lang.String, int, int)
	 */
	@Override
	public Set<FeedItem> findInDescription(String regexp, int limit, int offset)
	{
		return findInDescription(regexp, limit, offset, null);
	}
	
	/*
	 * (non-Javadoc)
	 * @see yarr.stor.dao.FeedItemDAO#findInDescription(java.lang.String, int, int, java.util.List)
	 */
	@Override
	public Set<FeedItem> findInDescription(String regexp, int limit, int offset, final List<String> feeds)
	{
		BasicDBObject query = new BasicDBObject("description", Pattern.compile(regexp, Pattern.CASE_INSENSITIVE));
		DBCursor cursor = getItemCollection().find(query);
		return getDataFromCursor(cursor, limit, offset, new ItemMatch()
		{
			@Override
			public boolean itemMatches(FeedItem item)
			{
				// Check if the feed id is in the list of feeds.
				return feeds == null || feeds.size() == 0 || feeds.contains(item.getFeedId());
			}
		});
	}
	

	/* (non-Javadoc)
	 * @see yarr.stor.dao.FeedItemDAO#findItemsBeforePubDate(java.util.Date)
	 */
	@Override
	public Set<FeedItem> findItemsBeforePubDate(Date date)
	{
		return findItemsBeforePubDate(date, 0);
	}
	
	/* (non-Javadoc)
	 * @see yarr.stor.dao.FeedItemDAO#findItemsBeforePubDate(java.util.Date, int)
	 */
	@Override
	public Set<FeedItem> findItemsBeforePubDate(Date date, int limit)
	{
		BasicDBObject query = new BasicDBObject("pubdate", new BasicDBObject("$lt", date));
		DBCursor cursor = getItemCollection().find(query);
		return getDataFromCursor(cursor, limit, 0, null);
	}
	
	/**
	 * FeedItem comparator. Necessary for adding items to the set created in getFeedItemsForFeed()
	 * @author Johan Mulder <johan@mulder.net>
	 */
	private static class FeedItemComparator implements Comparator<FeedItem> 
	{
		// Order by these fields.
		enum OrderField
		{
			TIME;
		}
		// Order in these directions.
		enum OrderDirection
		{
			ASCENDING, DESCENDING;
		}
		// The field to order by.
		private final OrderField orderField;
		// The sort direction.
		private final OrderDirection orderDirection;
		
		FeedItemComparator(OrderField orderBy, OrderDirection sortDirection)
		{
			// descending 
			this.orderField = orderBy;
			this.orderDirection = sortDirection;
		}
		
		@Override
		public int compare(FeedItem o1, FeedItem o2)
		{
			Date date1 = o1.getPublishDate();
			Date date2 = o2.getPublishDate();
			// Ignore this entry if one of the dates is unavailable.
			if (date1 == null || date2 == null)
				return -1;
			
			// Order by or direction is irrelevant if the objects are equal.
			if (date1.equals(date2))
				return 0;
			
			switch (orderField)
			{
				case TIME:
					
					if (orderDirection == OrderDirection.ASCENDING)
					{
						if (date1.after(date2))
							return -1;
						return 1;
					}
					else if (orderDirection == OrderDirection.DESCENDING)
					{
						if (date1.after(date2))
							return 1;
						return -1;
					}
					break;
			}

			// Default.
			return 0;
		}
	}
}
