/**
 * 
 * File created at 3 apr. 2013 22:33:02 by Johan Mulder <johan@mulder.net>
 */
package yarr.stor.dao.impl;

import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.Properties;

import javax.naming.AuthenticationException;

import yarr.stor.dao.MongoDbUtil;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.WriteConcern;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class MongoDbUtilImpl implements MongoDbUtil
{
	private MongoClient client = null;
	private DB db = null;
	private String hostname = null;
	private String dbName = null;
	private String username = null;
	private String password = null;
	
	public MongoDbUtilImpl(String hostname, String dbName) throws AuthenticationException, UnknownHostException
	{
		this.hostname = hostname;
		this.dbName = dbName;
		connect();
	}
	
	public MongoDbUtilImpl(String hostname, String dbName, String username, String password) throws AuthenticationException, UnknownHostException
	{
		this.hostname = hostname;
		this.dbName = dbName;
		this.username = username;
		this.password = password;
		connect();
	}
	
	public MongoDbUtilImpl() throws UnknownHostException, AuthenticationException
	{
		Properties connProps = new Properties();
		InputStream input = null;
		try
		{
			// Attempt to look up the mongodb.properties in the class path itself.
			input = getClass().getResourceAsStream("/mongodb.properties");
			connProps.load(input);
			// Set properties.
			this.username = connProps.getProperty("yarr.store.username");
			this.password = connProps.getProperty("yarr.store.password");
			this.hostname = connProps.getProperty("yarr.store.hostname");
			this.dbName = connProps.getProperty("yarr.store.dbname");
			connect();
		}
		catch (IOException e)
		{
			throw new AuthenticationException("Unable to load mongodb.properties file from class path: " + e.getMessage());
		}
		finally
		{
			try
			{
				if (input != null)
					input.close();
			}
			catch (IOException e)
			{
				// No worries.
			}
		}
	}
	
	private void connect() throws UnknownHostException, AuthenticationException
	{
		client = new MongoClient(hostname);
		// We need to wait until data has been committed.
		// TODO: Make this configurable.
		client.setWriteConcern(WriteConcern.ACKNOWLEDGED);
		db = client.getDB(dbName);
		if (username != null && username.trim().length() > 0)
		{
			String password = this.password == null ? this.password : "";
			if (!db.authenticate(username.trim(), password.trim().toCharArray()))
				throw new AuthenticationException("Authentication to host " + hostname + " failed for user " + username);
		}
	}
	
	/* (non-Javadoc)
	 * @see yarr.stor.dao.MongoDbUtil#getDB()
	 */
	@Override
	public DB getDB()
	{
		return db;
	}
	
	/*
	 * (non-Javadoc)
	 * @see yarr.stor.dao.MongoDbUtil#getClient()
	 */
	@Override
	public MongoClient getClient()
	{
		return client;
	}
	
	/* (non-Javadoc)
	 * @see yarr.stor.dao.MongoDbUtil#getCollection(java.lang.String)
	 */
	@Override
	public DBCollection getCollection(String name)
	{
		return db.getCollection(name);
	}

}
