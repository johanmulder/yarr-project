package yarr.stor.dao.impl;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import yarr.stor.dao.FeedDAO;
import yarr.stor.dao.FeedItemDAO;
import yarr.stor.dao.MongoDbUtil;
import yarr.stor.domain.Feed;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

public class FeedDAOImpl implements FeedDAO
{
	// Logging object.
	private static final Logger logger = LoggerFactory.getLogger(FeedDAOImpl.class);
	// The name of the feed collection.
	public static final String FEED_COLLECTION = "feed";
	// Mongo db utility object.
	private MongoDbUtil mongoDbUtil = null;
	// Feed collection.
	private DBCollection feedCollection = null;
	// Feed item DAO necessary to retrieve the number of items in this feed.
	private FeedItemDAO feedItemDAO;
	
	/**
	 * Class constructor.
	 * @param mongoDbUtil
	 * @param feedItemDAO
	 */
	public FeedDAOImpl(MongoDbUtil mongoDbUtil, FeedItemDAO feedItemDAO)
	{
		this.mongoDbUtil = mongoDbUtil;
		this.feedItemDAO = feedItemDAO;
	}
	
	/**
	 * Get the feed collection.
	 * @return
	 */
	private DBCollection getFeedCollection()
	{
		if (feedCollection == null)
			feedCollection = mongoDbUtil.getCollection(FEED_COLLECTION);
		return feedCollection;
	}

	/*
	 * (non-Javadoc)
	 * @see yarr.stor.dao.FeedDAO#getFeedByUrl(java.net.URL)
	 */
	@Override
	public Feed getFeedByFeedUrl(URL url)
	{
		return getFeedByFeedUrl(url.toExternalForm());
	}

	/*
	 * (non-Javadoc)
	 * @see yarr.stor.dao.FeedDAO#getFeedByUrl(java.lang.String)
	 */
	@Override
	public Feed getFeedByFeedUrl(String url)
	{
		DBCursor cursor = getFeedCollection().find(new BasicDBObject("feed_url", url));
		DBObject object = null;
		try
		{
			if (cursor.hasNext())
			{
				object = cursor.next();
				// Convert to a feed object.
				return DataConverter.dataToFeed(object);
			}
		}
		catch (MalformedURLException e)
		{
			// This can't possibly be happening, because the save method in this class should
			// already have run into this.
			if (object != null)
				logger.error("Invalid URL found in object {}: {}", object.get("_id"), object.get("feed_url"));
		}
		finally
		{
			// Always close the cursor.
			cursor.close();
		}
		
		return null;
	}
	
	/**
	 * Get a DBobject by id.
	 * @param id
	 * @return
	 */
	private DBObject getDataObjectById(String id)
	{
		return getFeedCollection().findOne(new ObjectId(id));
	}
	
	/*
	 * (non-Javadoc)
	 * @see yarr.stor.dao.FeedDAO#getFeedById(long)
	 */
	@Override
	public Feed getFeedById(String id)
	{
		DBObject object = getDataObjectById(id);
		if (object == null)
			return null;
		try
		{
			Feed feed = DataConverter.dataToFeed(object);
			feed.setNumFeedItems(feedItemDAO.countNumFeedItems(feed));
			return feed;
		}
		catch (MalformedURLException e)
		{
			logger.error("Feed ID {} has an invalid url stored", id, e);
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see yarr.stor.dao.FeedDAO#save(yarr.stor.domain.Feed)
	 */
	@Override
	public void create(Feed feed)
	{
		DBObject dbobject = DataConverter.feedToData(feed);
		/* WriteResult result = */ getFeedCollection().save(dbobject);
		ObjectId objectId = (ObjectId) dbobject.get("_id");
		feed.setId(objectId.toString());
	}
	
	/*
	 * (non-Javadoc)
	 * @see yarr.stor.dao.FeedDAO#update(yarr.stor.domain.Feed)
	 */
	@Override
	public void update(Feed feed)
	{
		DBObject dbobject = DataConverter.feedToData(feed);
		// These need to be skipped.
		dbobject.removeField("_id");
		dbobject.removeField("icon");
		getFeedCollection().update(new BasicDBObject("_id", new ObjectId(feed.getId())), dbobject);
	}

	@Override
	public void delete(Feed feed)
	{
		delete(feed.getId());
	}
	
	@Override
	public void delete(String id)
	{
		DBObject object = getDataObjectById(id);
		getFeedCollection().remove(object);
	}

	/*
	 * (non-Javadoc)
	 * @see yarr.stor.dao.FeedDAO#getOverdueFeeds()
	 */
	@Override
	public Set<Feed> getOverdueFeeds()
	{
		return getOverdueFeeds(0);
	}
	
	/*
	 * (non-Javadoc)
	 * @see yarr.stor.dao.FeedDAO#getOverdueFeeds(int)
	 */
	@Override
	public Set<Feed> getOverdueFeeds(int limit)
	{
		DBCursor cursor = getFeedCollection().find();
		Set<Feed> feeds = new HashSet<Feed>();
		try
		{
			int added = 0;
			Date now = new Date();
			while (cursor.hasNext() && (limit <= 0 || ++added == limit))
			{
				DBObject object = null;
				try
				{
					object = cursor.next();
					Feed feed = DataConverter.dataToFeed(object);
					Feed.FetchInfo fetchInfo = feed.getFetchInfo();
					if (fetchInfo == null || fetchInfo.getLastFetched() == null)
						// Always add if there's no fetch information available.
						feeds.add(feed);
					else
					{
						// If the due date is before "now", the feed needs to be refreshed.
						Date due = new Date(fetchInfo.getLastFetched().getTime() + (feed.getTtl() * 1000));
						if (due.before(now))
							feeds.add(feed);
					}
				}
				catch (MalformedURLException e)
				{
					if (object != null)
						logger.error("feed {} has an invalid url", object.get("_id").toString());
				}
			}
		}
		finally
		{
			cursor.close();
		}
		
		return feeds;
	}
}
