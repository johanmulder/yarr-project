package yarr.stor.dao.impl;

import yarr.stor.dao.DAOFacade;
import yarr.stor.dao.FeedDAO;
import yarr.stor.dao.FeedItemDAO;
import yarr.stor.dao.MongoDbUtil;

public class DAOFacadeImpl implements DAOFacade
{
	// MongoDB utility class.
	private MongoDbUtil mongoDbUtil = null;
	// DAO objects.
	private FeedDAO feedDAO = null;
	private FeedItemDAO feedItemDAO = null;
	
	/**
	 * Class constructor.
	 * @param mongoDbUtil Mandatory MongoDbUtil object.
	 */
	public DAOFacadeImpl(MongoDbUtil mongoDbUtil)
	{
		this.mongoDbUtil = mongoDbUtil;
	}

	/*
	 * (non-Javadoc)
	 * @see yarr.stor.dao.DAOFacade#getFeedDAO()
	 */
	@Override
	public FeedDAO getFeedDAO()
	{
		// Initialize if not done yet.
		if (feedDAO == null)
			feedDAO = new FeedDAOImpl(mongoDbUtil, getFeedItemDAO());
		return feedDAO;
	}

	/*
	 * (non-Javadoc)
	 * @see yarr.stor.dao.DAOFacade#getFeedItemDAO()
	 */
	@Override
	public FeedItemDAO getFeedItemDAO()
	{
		if (feedItemDAO == null)
			return feedItemDAO = new FeedItemDAOImpl(mongoDbUtil);
		return feedItemDAO;
	}

}
