package yarr.stor.dao.impl;

import java.net.MalformedURLException;
import java.util.Date;

import org.bson.types.ObjectId;

import yarr.stor.domain.Feed;
import yarr.stor.domain.FeedItem;
import yarr.stor.domain.FeedType;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

class DataConverter
{
	/**
	 * Convert a DB object to a feed object.
	 * This method is useful when converting retrieved feed data to an object
	 * suitable for persisting. 
	 * @param object
	 * @return
	 * @throws MalformedURLException 
	 */
	public static Feed dataToFeed(DBObject object) throws MalformedURLException
	{
		// Sanity check.
		if (object == null)
			return null;

		// Initialize return data.
		Feed feed = new Feed();
		
		// Extract the data.
		ObjectId id = (ObjectId) object.get("_id");
		feed.setId(id != null ? id.toString() : null); 
		feed.setDescription((String) object.get("description"));
		feed.setLanguage((String) object.get("language"));
		feed.setLastUpdated((Date) object.get("last_updated"));
		feed.setPublishDate((Date) object.get("pubdate"));
		feed.setTitle((String) object.get("title"));
		feed.setTtl(object.get("ttl") == null ? 3600 : (Integer) object.get("ttl"));
		feed.setType(object.get("type") != null ? FeedType.fromString((String) object.get("type")) : null);
		feed.setUrl((String) object.get("url"));
		feed.setFeedUrl((String) object.get("feed_url"));
		// Icon.
		feed.setIcon(getIconFromObject(object));
		// Fetch information
		feed.setFetchInfo(getFetchInfoFromObject(object));
		
		return feed;
	}
	
	/**
	 * Get icon data from a DBObject.
	 * @param object
	 * @return
	 */
	private static Feed.Icon getIconFromObject(DBObject object)
	{
		// Sanity check.
		if (object == null)
			return null;
		DBObject objIcon = (DBObject) object.get("icon");
		if (objIcon == null)
			return null;
		return new Feed.Icon((String) objIcon.get("mime_type"), 
				(byte[]) objIcon.get("icon_image"), 
				(Integer) objIcon.get("size"));
	}
	
	/**
	 * Get fetch information from a DBObject.
	 * @param object
	 * @return
	 */
	private static Feed.FetchInfo getFetchInfoFromObject(DBObject object)
	{
		if (object == null)
			return null;
		
		DBObject fetch = (DBObject) object.get("fetch_info");
		if (fetch == null)
			return null;
		
		return new Feed.FetchInfo((Date) fetch.get("last_fetched"), 
				(Integer) fetch.get("status"), (String) fetch.get("last_status"));
	}
	
	/**
	 * Convert a feed object to a mongodb DBObject.
	 * @param feed
	 * @return
	 */
	public static DBObject feedToData(Feed feed)
	{
		// Sanity check.
		if (feed == null)
			return null;
		
		BasicDBObject object = new BasicDBObject();
		object.append("description", feed.getDescription());
		object.append("language", feed.getLanguage());
		object.append("last_updated", feed.getLastUpdated());
		object.append("pubdate", feed.getPublishDate());
		object.append("title", feed.getTitle());
		object.append("ttl", feed.getTtl());
		object.append("type", feed.getType() != null ? feed.getType().toString() : null);
		object.append("url", feed.getUrl() != null ? feed.getUrl().toExternalForm() : null);
		object.append("feed_url", feed.getFeedUrl() != null ? feed.getFeedUrl().toExternalForm() : null);
		if (feed.getId() != null)
			object.append("_id", new ObjectId(feed.getId()));
		// Icon.
		if (feed.getIcon() != null)
			object.append("icon", getObjectFromIcon(feed.getIcon()));
		// Fetch information.
		if (feed.getFetchInfo() != null)
			object.append("fetch_info", getObjectFromFetchInfo(feed.getFetchInfo()));
		return object;
	}
	
	private static DBObject getObjectFromIcon(Feed.Icon icon)
	{
		if (icon == null)
			return null;
		
		return new BasicDBObject()
			.append("mime_type", icon.getMimeType())
			.append("icon_image", icon.getImage())
			.append("size", icon.getSize())
		;
	}
	
	/**
	 * Get 
	 * @param fetchInfo
	 * @return
	 */
	private static DBObject getObjectFromFetchInfo(Feed.FetchInfo fetchInfo)
	{
		if (fetchInfo == null)
			return null;
		
		return new BasicDBObject()
			.append("last_fetched", fetchInfo.getLastFetched())
			.append("status", fetchInfo.getStatus())
			.append("last_status", fetchInfo.getLastResult())
		;
	}
	
	/**
	 * Convert a data object to a feed item object. 
	 * @param object
	 * @return
	 * @throws MalformedURLException 
	 */
	public static FeedItem dataToItem(DBObject object) throws MalformedURLException
	{
		// Sanity check.
		if (object == null)
			return null;
		
		FeedItem item = new FeedItem();
		// Set author.
		DBObject author = (DBObject) object.get("author");
		if (author != null)
		{
			FeedItem.Author a = new FeedItem.Author();
			a.setName((String) author.get("name")); 
			a.setEmail((String) author.get("email"));
			item.setAuthor(a);
		}
		// All other items.
		ObjectId id = (ObjectId) object.get("_id");
		item.setId(id != null ? id.toString() : null); 
		item.setDescription((String) object.get("description"));
		item.setGuid((String) object.get("guid"));
		item.setLastUpdated((Date) object.get("last_updated"));
		item.setPublishDate((Date) object.get("pubdate"));
		item.setTitle((String) object.get("title"));
		item.setUrl((String) object.get("url"));
		ObjectId feedId = (ObjectId) object.get("feed_id");
		item.setFeedId(feedId != null ? feedId.toString() : null);
		
		return item;
	}
	
	/**
	 * Convert an item to a dbobject suitable for persisting.
	 * @param item
	 * @return
	 */
	public static DBObject itemToData(FeedItem item)
	{
		// Sanity check.
		if (item == null)
			return null;

		BasicDBObject dbobject = new BasicDBObject()
			.append("author", new BasicDBObject().append("name", item.getAuthor().getName()).append("email", item.getAuthor().getEmail()))
			.append("description", item.getDescription())
			.append("guid", item.getGuid())
			.append("last_updated", item.getLastUpdated())
			.append("pubdate", item.getPublishDate())
			.append("title", item.getTitle())
			.append("url", item.getUrl() != null ? item.getUrl().toExternalForm() : null)
		;
		if (item.getFeedId() != null)
			dbobject.append("feed_id", new ObjectId(item.getFeedId()));
		if (item.getId() != null)
			dbobject.append("_id", new ObjectId(item.getId()));
		return dbobject;
	}
}
