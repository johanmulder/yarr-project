package yarr.stor.dao;

import java.net.URL;
import java.util.Set;

import yarr.stor.domain.Feed;

/**
 * This interface defines the DAO for fetching, searching and manipulating
 * feed items in the data store.
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public interface FeedDAO
{
	/**
	 * Get a feed object by URL
	 * @param url
	 * @return
	 */
	public Feed getFeedByFeedUrl(URL url);
	
	/**
	 * Get a feed object by URL
	 * @param url
	 * @return
	 */
	public Feed getFeedByFeedUrl(String url);
	
	/**
	 * Get a set of overdue feeds. This means fetching all feeds which need to
	 * be refreshed or fetched for the first time.
	 * @return
	 */
	public Set<Feed> getOverdueFeeds();
	
	/**
	 * Get a set of overdue feeds. This means fetching all feeds which need to
	 * be refreshed or fetched for the first time.
	 * @param limit Maximize the number of results.
	 * @return
	 */
	public Set<Feed> getOverdueFeeds(int limit);
	
	/**
	 * Get a feed object by ID.
	 * @param id
	 * @return
	 */
	public Feed getFeedById(String id);
	
	/**
	 * Save a feed to the database.
	 * @param feed
	 */
	public void create(Feed feed);
	
	/**
	 * Update a feed.
	 * @param feed
	 */
	public void update(Feed feed);
	
	/**
	 * Delete a feed.
	 * @param feed
	 */
	public void delete(Feed feed);
	
	/**
	 * Delete a feed object by id.
	 * @param id
	 */
	public void delete(String id);
}
