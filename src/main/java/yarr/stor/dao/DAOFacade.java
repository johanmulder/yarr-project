package yarr.stor.dao;

public interface DAOFacade
{
	/**
	 * Get a FeedDAO object.
	 * @return
	 */
	public FeedDAO getFeedDAO();
	
	/**
	 * Get a FeedItemDAO object.
	 * @return
	 */
	public FeedItemDAO getFeedItemDAO();
}
