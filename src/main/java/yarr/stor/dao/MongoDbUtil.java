package yarr.stor.dao;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;

public interface MongoDbUtil
{
	/**
	 * Get a DB object.
	 * @return
	 */
	public DB getDB();
	
	/**
	 * Get a collection from the current DB.
	 * @param name
	 * @return
	 */
	public DBCollection getCollection(String name);
	
	/**
	 * Return the MongoClient object.
	 * @return
	 */
	public MongoClient getClient();
}
