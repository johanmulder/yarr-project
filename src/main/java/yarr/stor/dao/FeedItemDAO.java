package yarr.stor.dao;

import java.util.Date;
import java.util.List;
import java.util.Set;

import yarr.stor.domain.Feed;
import yarr.stor.domain.FeedItem;

/**
 * This interface defines the DAO for fetching, searching and manipulating
 * feed items in the data store.
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public interface FeedItemDAO
{
	/**
	 * Get a feed item by id.
	 * @param id
	 * @return Returns the feed item, or null if no entries.
	 */
	public FeedItem getById(String id);
	/**
	 * Get all feed items for a given feed.
	 * @param feed
	 * @return Returns the set of feed items, or null if no entries.
	 */
	public Set<FeedItem> getFeedItemsForFeed(Feed feed);
	/**
	 * Get all feed items for a given feed, with a limited amount.
	 * @param feed
	 * @param limit
	 * @return Returns the set of feed items, or null if no entries.
	 */
	public Set<FeedItem> getFeedItemsForFeed(Feed feed, int limit);
	/**
	 * Get all feed items for a given feed, with a limited amount at the specified offset
	 * @param feed
	 * @param limit
	 * @param offset
	 * @return Returns the set of feed items, or null if no entries.
	 */
	public Set<FeedItem> getFeedItemsForFeed(Feed feed, int limit, int offset);
	/**
	 * Get all feed items for a given feed by feed id string.
	 * @param feedId
	 * @return Returns the set of feed items, or null if no entries.
	 */
	public Set<FeedItem> getFeedItemsForFeed(String feedId);
	/**
	 * Get all feed items for a given feed by feed id with a limited amount.
	 * @param feedId
	 * @param limit
	 * @return Returns the set of feed items, or null if no entries.
	 */
	public Set<FeedItem> getFeedItemsForFeed(String feedId, int limit);
	/**
	 * Get all feed items for a given feed, with a limited amount at the specified offset
	 * @param feedId
	 * @param limit
	 * @param offset
	 * @return Returns the set of feed items, or null if no entries.
	 */
	public Set<FeedItem> getFeedItemsForFeed(String feedId, int limit, int offset);
	/**
	 * Create a new feed item.
	 * @param feedItem
	 */
	public void create(FeedItem feedItem);
	/**
	 * Update a feed item.
	 * @param feedItem
	 */
	public void update(FeedItem feedItem);
	/**
	 * Delete a feed item.
	 * @param feedItem
	 */
	public void delete(FeedItem feedItem);
	/**
	 * Delete a feed item by string.
	 * @param id
	 */
	public void delete(String id);
	/**
	 * Count the number of feed items for a given feed.
	 * @param feed
	 * @return -1 if no feeds found, the number of itmes otherwise.
	 */
	public int countNumFeedItems(Feed feed);
	/**
	 * Find feed items with the matching description
	 * @param regexp A regular expression to match with.
	 * @return Returns the set of feed items, or null if no entries.
	 */
	public Set<FeedItem> findInDescription(String regexp);
	/**
	 * Find feed items with the matching description
	 * @param regexp A regular expression to match with.
	 * @param limit Return a maximum of this number of entries.
	 * @return Returns the set of feed items, or null if no entries.
	 */
	public Set<FeedItem> findInDescription(String regexp, int limit);
	/**
	 * Find feed items with the matching description
	 * @param regexp A regular expression to match with.
	 * @param limit Return a maximum of this number of entries.
	 * @param offset Skip this number of entries in the search result.
	 * @return Returns the set of feed items, or null if no entries.
	 */
	public Set<FeedItem> findInDescription(String regexp, int limit, int offset);
	/**
	 * Find feed items with the matching description in the given list of feed ids
	 * @param regexp
	 * @param limit
	 * @param offset
	 * @param feeds
	 * @return Returns the set of feed items, or null if no entries.
	 */
	public Set<FeedItem> findInDescription(String regexp, int limit, int offset, List<String> feeds);
	/**
	 * Find all items before a given date.
	 * @param date
	 * @return Returns the set of feed items, or null if no entries.
	 */
	public Set<FeedItem> findItemsBeforePubDate(Date date);
	/**
	 * Find all items before a given date.
	 * @param date
	 * @param limit Limit to this amount of entries.
	 * @return Returns the set of feed items, or null if no entries.
	 */
	public Set<FeedItem> findItemsBeforePubDate(Date date, int limit);

}
