package yarr.stor.domain;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

/**
 * Item stored in a feed. This data class defines the data members for items
 * which are connected to a feed.
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class FeedItem
{
	// Database data.
	private String id;
	// Item data.
	private String feedId;
	private String title;
	private String description;
	private URL url;
	private String guid;
	private Author author;
	private Date publishDate;
	private Date lastUpdated;

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}
	
	public String getFeedId()
	{
		return feedId;
	}
	
	public void setFeedId(String feedId)
	{
		this.feedId = feedId;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public URL getUrl()
	{
		return url;
	}

	public void setUrl(URL url)
	{
		this.url = url;
	}
	
	/**
	 * Set the URL of the feed by giving a String.
	 * @param url
	 * @throws MalformedURLException
	 */
	public void setUrl(String url) throws MalformedURLException
	{
		this.url = new URL(url);
	}

	public String getGuid()
	{
		return guid;
	}

	public void setGuid(String guid)
	{
		this.guid = guid;
	}

	public Author getAuthor()
	{
		return author;
	}

	public void setAuthor(Author author)
	{
		this.author = author;
	}

	public Date getPublishDate()
	{
		return publishDate;
	}

	public void setPublishDate(Date publishDate)
	{
		this.publishDate = publishDate;
	}

	public Date getLastUpdated()
	{
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated)
	{
		this.lastUpdated = lastUpdated;
	}
	
	/**
	 * Author data type.
	 * @author Johan Mulder <johan@mulder.net>
	 */
	public static class Author
	{
		// Author data.
		private String name;
		private String email;
		
		/**
		 * Empty class constructor.
		 */
		public Author()
		{
			
		}
		
		/**
		 * Class constructor with name and email.
		 * @param name
		 * @param email
		 */
		public Author(String name, String email)
		{
			this.name = name;
			this.email = email;
		}

		public String getName()
		{
			return name;
		}

		public void setName(String name)
		{
			this.name = name;
		}

		public String getEmail()
		{
			return email;
		}

		public void setEmail(String email)
		{
			this.email = email;
		}
		
	}


}
