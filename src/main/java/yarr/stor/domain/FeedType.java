package yarr.stor.domain;

/**
 * Enum describing the type of a feed.
 * @author Johan Mulder <johan@mulder.net>
 */
public enum FeedType
{
	
	RSS("rss"), 	// RSS feeds
	ATOM("atom"),	// Atom feeds.
	UNKNOWN("unknown");
	private String value;

	/**
	 * Private enum constructor.
	 * @param s
	 */
	private FeedType(String s)
	{
		value = s;
	}
	
	/**
	 * Get the feed type enum value as a string.
	 */
	@Override
	public String toString()
	{
		return value;
	}
	
	/**
	 * Return a FeedType enumerate by looking up the enum by String.
	 * @param e The string value of 
	 * @return
	 */
	public static FeedType fromString(String e)
	{
		for (FeedType f : FeedType.values())
			if (e.equalsIgnoreCase(f.toString()))
				return f;
		return UNKNOWN;
	}
}
