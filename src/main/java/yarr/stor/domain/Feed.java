package yarr.stor.domain;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

/**
 * Data class for storing a feed
 * @author Johan Mulder <johan@mulder.net>
 */
public class Feed
{
	// Storage data
	private String id;
	// Mandatory feed data.
	private String title;
	private URL feedUrl;
	private URL url;
	private String description;
	// Optional feed data.
	private String language;
	private Date publishDate;
	private Date lastUpdated;
	private int ttl = 3600;
	private FeedType type;
	private FetchInfo fetchInfo;
	private Icon icon;
	private int numFeedItems = 0;

	/**
	 * Get the database identifier of this feed.
	 * @return
	 */
	public String getId()
	{
		return id;
	}

	/**
	 * Set database identifier. Don't do this for new feeds.
	 * @param id
	 */
	public void setId(String id)
	{
		this.id = id;
	}

	/**
	 * Get the feed title.
	 * @return
	 */
	public String getTitle()
	{
		return title;
	}

	/**
	 * Set the feed title.
	 * @param title
	 */
	public void setTitle(String title)
	{
		this.title = title;
	}

	/**
	 * Get the URL of the feed.
	 * @return
	 */
	public URL getUrl()
	{
		return url;
	}

	/**
	 * Set the URL of the feed.
	 * @param url
	 */
	public void setUrl(URL url)
	{
		this.url = url;
	}

	/**
	 * Set the URL of the feed by giving a String.
	 * @param url
	 * @throws MalformedURLException
	 */
	public void setUrl(String url) throws MalformedURLException
	{
		if (url == null || url.trim().length() == 0)
			this.url = null;
		else
			this.url = new URL(url);    
	}
	
	/**
	 * Get the feed URL.
	 * @return
	 */
	public URL getFeedUrl()
	{
		return feedUrl;
	}
	
	/**
	 * Set the feed url.
	 * @param url
	 */
	public void setFeedUrl(URL url)
	{
		this.feedUrl = url;
	}
	
	/**
	 * Set the feed URL by string.
	 * @param url
	 * @throws MalformedURLException
	 */
	public void setFeedUrl(String url) throws MalformedURLException
	{
		this.feedUrl = new URL(url);
	}

	/**
	 * Set the feed description.
	 * @return
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * Set the feed description.
	 * @param description
	 */
	public void setDescription(String description)
	{
		this.description = description;
	}

	/**
	 * Get the feed language (optional)
	 * @return
	 */
	public String getLanguage()
	{
		return language;
	}

	/**
	 * Set the feed language.
	 * @param language
	 */
	public void setLanguage(String language)
	{
		this.language = language;
	}

	/**
	 * Get the (optional) publish date of the feed 
	 * @return
	 */
	public Date getPublishDate()
	{
		return publishDate;
	}

	/**
	 * Set the (optional) publush date of the feed.
	 * @param publishDate
	 */
	public void setPublishDate(Date publishDate)
	{
		this.publishDate = publishDate;
	}

	/**
	 * Get the last update time of the field.
	 * @return
	 */
	public Date getLastUpdated()
	{
		return lastUpdated;
	}

	/**
	 * Set the last update time of the feed.
	 * @param lastUpdated
	 */
	public void setLastUpdated(Date lastUpdated)
	{
		this.lastUpdated = lastUpdated;
	}

	/**
	 * Get the TTL of a feed in seconds (default 3600)
	 * @return
	 */
	public int getTtl()
	{
		return ttl;
	}

	/**
	 * Set the TTL of a feed in seconds.
	 * @param ttl
	 */
	public void setTtl(int ttl)
	{
		this.ttl = ttl;
	}
	
	/**
	 * Get the feed type.
	 * @return
	 */
	public FeedType getType()
	{
		return type;
	}
	
	/**
	 * Set the feed type.
	 * @param type
	 */
	public void setType(FeedType type)
	{
		this.type = type;
	}
	
	/**
	 * Get the fetch information.
	 * @return
	 */
	public FetchInfo getFetchInfo()
	{
		return fetchInfo;
	}
	
	/**
	 * Set the fetch information.
	 * @param fetchInfo
	 */
	public void setFetchInfo(FetchInfo fetchInfo)
	{
		this.fetchInfo = fetchInfo;
	}
	
	/**
	 * Set the icon for this feed.
	 * @return
	 */
	public Icon getIcon()
	{
		return icon;
	}
	
	/**
	 * Get the icon for this feed.
	 * @param icon
	 */
	public void setIcon(Icon icon)
	{
		this.icon = icon;
	}
	
	/**
	 * Set the number of feed items for this feed
	 * @param items
	 */
	public void setNumFeedItems(int items)
	{
		numFeedItems = items;
	}
	
	/**
	 * Get the number of feed items for this feed.
	 * @return
	 */
	public int getNumFeedItems()
	{
		return numFeedItems;
	}
	
	
	/**
	 * Information regarding the fetching of the feed.
	 * @author Johan Mulder <johan@mulder.net>
	 */
	public static class FetchInfo
	{
		private Date lastFetched;
		private int status;
		private String lastResult;
		
		/**
		 * Convenience constructor.
		 * @param lastFetched
		 * @param status
		 * @param lastResult
		 */
		public FetchInfo(Date lastFetched, int status, String lastResult)
		{
			this.lastFetched = lastFetched;
			this.status = status;
			this.lastResult = lastResult;
		}
		
		public FetchInfo()
		{
			
		}
		
		public int getStatus()
		{
			return status;
		}

		public void setStatus(int status)
		{
			this.status = status;
		}

		public String getLastResult()
		{
			return lastResult;
		}

		public void setLastResult(String lastResult)
		{
			this.lastResult = lastResult;
		}

		/**
		 * Get the last fetched time.
		 * @return
		 */
		public Date getLastFetched()
		{
			return lastFetched;
		}
		
		/**
		 * Set the last fetched time.
		 * @param lastFetched
		 */
		public void setLastFetched(Date lastFetched)
		{
			this.lastFetched = lastFetched;
		}
	}
	
	
	/**
	 * Binary icon of the feed.
	 * @author Johan Mulder <johan@mulder.net>
	 */
	public static class Icon
	{
		// The mime type of the icon.
		private String mimeType;
		// The binary image data
		private byte[] image;
		// The image size.
		private int size;

		/**
		 * Convenience constructor.
		 * @param mimeType
		 * @param image
		 * @param size
		 */
		public Icon(String mimeType, byte[] image, int size)
		{
			this.mimeType = mimeType;
			this.image = image;
			this.size = size;
		}
		
		public Icon()
		{
		}

		public String getMimeType()
		{
			return mimeType;
		}

		public void setMimeType(String mimeType)
		{
			this.mimeType = mimeType;
		}

		public byte[] getImage()
		{
			return image;
		}

		public void setImage(byte[] image)
		{
			this.image = image;
		}

		public int getSize()
		{
			return size;
		}

		public void setSize(int size)
		{
			this.size = size;
		}
	}
}
