package yarr.feed.html;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

public class HTMLSanitizerTest extends TestCase
{
	private HTMLSanitizer sanitizer = null;

	@Override
	@Before
	protected void setUp() throws Exception
	{
		super.setUp();
		sanitizer = new HTMLSanitizer();
	}
	
	@Test
	public void testStripOnclick()
	{
		String input = "<div onclick=\"javascript: window.alert('bla');\">test</div>";
		assertEquals(sanitizer.sanitize(input, "http://localhost/"), "<div>test</div>");
	}

	@Test
	public void testKeepStyle()
	{
		String input = "<div style=\"bla\">test</div>";
		assertEquals(sanitizer.sanitize(input, "http://localhost/"), input);
	}
	
}
