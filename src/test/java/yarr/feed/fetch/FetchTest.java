package yarr.feed.fetch;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import junit.framework.TestCase;

import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;

public class FetchTest extends TestCase
{

	public void testFetch() throws IllegalArgumentException, FeedException, IOException
	{
		// https://rometools.jira.com/
		XmlReader reader = null;
		try
		{
			URL feedSource = new URL("http://www.localhost.nl/~johan/school/Thema_3.1/Project/ars.xml");
	        SyndFeedInput input = new SyndFeedInput();
	        reader = new XmlReader(feedSource);
	        SyndFeed feed = input.build(reader);
	        @SuppressWarnings("unchecked")
			List<SyndEntry> entries = (List<SyndEntry>) feed.getEntries();
	        for (SyndEntry entry : entries)
	        {
	        	System.out.println("author: " + entry.getAuthor() + 
	        			"\nlink:" + entry.getLink() + 
	        			"\ntitle: " + entry.getTitle() +
	        			"\npublish date: " + entry.getPublishedDate() +
	        			"\nContents: " + entry.getContents() +
	        			"\ndescription: " + entry.getDescription().getValue() +
	        			"\n"
	        			);
	        }
		}
		finally
		{
			if (reader != null)
				reader.close();
		}
	}
}
