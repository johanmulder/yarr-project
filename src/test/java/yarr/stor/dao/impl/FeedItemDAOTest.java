package yarr.stor.dao.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.net.MalformedURLException;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import yarr.stor.dao.FeedItemDAO;
import yarr.stor.dao.MongoDbUtil;
import yarr.stor.domain.FeedItem;

public class FeedItemDAOTest
{
	private static MongoDbUtil mongoDbUtil = null;
	private FeedItemDAO feedItemDAO = null;
	private static String feedId = "515885f49386f72f9e38f4c3";
	
	@BeforeClass
	public static void setupMongoDbUtil()
	{
		try
		{
			mongoDbUtil = new MongoDbUtilImpl();
		}
		catch (Exception e)
		{
			Assume.assumeNotNull(mongoDbUtil);
		}
	}
	
	@Before
	public void setUp()
	{
		feedItemDAO = new FeedItemDAOImpl(mongoDbUtil);
	}
	

	@Test
	public void testCreate() throws MalformedURLException
	{
		FeedItem feedItem = new FeedItem();
		feedItem.setAuthor(new FeedItem.Author("Test Author", "test@test"));
		feedItem.setDescription("Test description");
		feedItem.setGuid(UUID.randomUUID().toString());
		feedItem.setLastUpdated(new Date());
		feedItem.setPublishDate(new Date());
		feedItem.setTitle("Test title");
		feedItem.setUrl("http://test.test/item");
		feedItem.setFeedId(feedId);
		feedItemDAO.create(feedItem);
		assertNotNull(feedItem.getId());
		Set<FeedItem> items = feedItemDAO.getFeedItemsForFeed(feedId, 1);
		assertTrue(items.size() > 0);
		feedItemDAO.delete(feedItem);
	}
}
