package yarr.stor.dao.impl;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;

import java.net.MalformedURLException;
import java.util.Date;

import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import yarr.stor.dao.FeedDAO;
import yarr.stor.dao.MongoDbUtil;
import yarr.stor.domain.Feed;
import yarr.stor.domain.FeedType;

public class FeedDAOTest
{
	private static MongoDbUtil mongoDbUtil = null;
	private FeedDAO feedDAO = null;
	
	@BeforeClass
	public static void setupMongoDbUtil()
	{
		try
		{
			mongoDbUtil = new MongoDbUtilImpl();
		}
		catch (Exception e)
		{
			Assume.assumeNotNull(mongoDbUtil);
		}
	}
	
	@Before
	public void setUp()
	{
		feedDAO = new FeedDAOImpl(mongoDbUtil, new FeedItemDAOImpl(mongoDbUtil));
	}

	/**
	 * Test saving, getting and deleting of a feed object.
	 * Yes, I know tests should have a single responsibility, but I'm
	 * just lazy.
	 * @throws MalformedURLException
	 * @throws InterruptedException 
	 */
	@Test
	public void testSaveGetAndDelete() throws MalformedURLException, InterruptedException
	{
		String url = "http://test.test/test.rss";
		
		// Create a new feed object.
		Feed feed = new Feed();
		feed.setDescription("test feed");
		feed.setLanguage("nl_NL");
		feed.setLastUpdated(new Date());
		feed.setPublishDate(new Date());
		feed.setTitle("test title");
		feed.setTtl(86400);
		feed.setType(FeedType.RSS);
		feed.setUrl("http://test.test/");
		feed.setFeedUrl(url);
		feed.setIcon(new Feed.Icon("image/png", new byte[] { 1, 2, 3 }, 3));
		feed.setFetchInfo(new Feed.FetchInfo(new Date(), 1, "200 OK"));
		// Save it.
		feedDAO.create(feed);
		assertNotNull(feed.getId());
		
		// Test retrieval.
		feed = feedDAO.getFeedByFeedUrl(url);
		assertNotNull(feed);
		assertNotNull(feed.getId());
		assertEquals(feed.getFeedUrl().toExternalForm(), url);
		
		// Test deletion.
		feedDAO.delete(feed);
		assertNull(feedDAO.getFeedByFeedUrl(url));
	}
}
