package yarr.stor.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.net.MalformedURLException;
import java.util.Date;
import java.util.UUID;

import org.bson.types.ObjectId;
import org.junit.Test;

import yarr.stor.domain.Feed;
import yarr.stor.domain.FeedItem;
import yarr.stor.domain.FeedType;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

public class DataConverterTest
{

	@Test
	public void testDataToFeed()
	{
		// Create a test object to convert.
		BasicDBObject dbobject = new BasicDBObject();
		Date testDate = new Date();
		dbobject.append("pubdate", testDate);
		dbobject.append("last_updated", testDate);
		dbobject.append("last_fetched", testDate);
		dbobject.append("language", "nl_NL");
		dbobject.append("title", "test data");
		int ttl = 86400;
		dbobject.append("ttl", ttl);
		dbobject.append("type", "rss");
		dbobject.append("url", "http://www.test.test/");
		dbobject.append("feed_url", "http://www.test.test/test.rss");
		dbobject.append("description", "unit test description");
		// Icon
		dbobject.append("icon", 
				new BasicDBObject()
					.append("mime_type", "image/png")
					.append("size", 3)
					.append("icon_image", new byte[] { 1, 2, 3}));
		// Fetch info.
		dbobject.append("fetch_info",
				new BasicDBObject()
					.append("last_fetched", testDate)
					.append("status", 1)
					.append("last_status", "200 OK")
				);
		
		// Do the conversion.
		final Feed feed;
		try
		{
			feed = DataConverter.dataToFeed(dbobject);
		}
		catch (MalformedURLException e)
		{
			fail("URL set failed");
			return;
		}
		assertNotNull(feed);
		
		// Compare the result.
		compareFeedToDBObject(dbobject, feed);
	}

	@Test
	public void testFeedToData() throws MalformedURLException
	{
		// Create test data.
		Date testDate = new Date();
		Feed feed = new Feed();
		feed.setDescription("unit test description");
		feed.setLanguage("nl_NL");
		feed.setLastUpdated(testDate);
		feed.setPublishDate(testDate);
		feed.setTitle("unit test title");
		int ttl = 86400;
		feed.setTtl(ttl);
		feed.setType(FeedType.ATOM);
		feed.setUrl("http://test.test/test.rss");
		feed.setIcon(new Feed.Icon("image/png", new byte[] { 1, 2, 3 }, 3));
		feed.setFetchInfo(new Feed.FetchInfo(testDate, 1, "200 OK"));
		
		// Do the conversion.
		DBObject dbobject = DataConverter.feedToData(feed);
		
		// Compare the result.
		compareFeedToDBObject(dbobject, feed);
	}
	
	/**
	 * Compare a feed data object to a mongodb DBObject.
	 * @param dbobject
	 * @param feed
	 */
	private void compareFeedToDBObject(DBObject dbobject, Feed feed)
	{
		assertEquals((Date) dbobject.get("pubdate"), feed.getPublishDate());
		assertEquals((Date) dbobject.get("last_updated"), feed.getLastUpdated());
		assertEquals(dbobject.get("language"), feed.getLanguage());
		assertEquals(dbobject.get("title"), feed.getTitle());
		assertEquals(dbobject.get("ttl"), feed.getTtl());
		assertEquals(dbobject.get("type"), feed.getType().toString());
		assertEquals(dbobject.get("url"), feed.getUrl().toExternalForm());
		assertEquals(dbobject.get("description"), feed.getDescription());
		DBObject icon = (DBObject) dbobject.get("icon"); 
		assertEquals(icon.get("mime_type"), feed.getIcon().getMimeType());
		assertEquals(icon.get("icon_image"), feed.getIcon().getImage());
		assertEquals(icon.get("size"), feed.getIcon().getSize());
		DBObject fetchInfo = (DBObject) dbobject.get("fetch_info");
		assertEquals(fetchInfo.get("last_fetched"), feed.getFetchInfo().getLastFetched());
		assertEquals(fetchInfo.get("status"), feed.getFetchInfo().getStatus());
		assertEquals(fetchInfo.get("last_status"), feed.getFetchInfo().getLastResult());
	}

	@Test
	public void testDataToItem() throws MalformedURLException
	{
		Date testDate = new Date();
		String uuid = UUID.randomUUID().toString();
		BasicDBObject dbobject = new BasicDBObject();
		dbobject
			.append("author", new BasicDBObject().append("name", "JUnit").append("email", "test@test.test"))
			.append("description", "Test description")
			.append("guid", uuid)
			.append("last_update", testDate)
			.append("pubdate", testDate)
			.append("title", "Test title")
			.append("feed_id", new ObjectId("515886fc9386178d1f386782"))
			.append("url", "http://test.test/test.rss")
		;
		compareItemToDBObject(dbobject, DataConverter.dataToItem(dbobject));
	}

	@Test
	public void testItemToData() throws MalformedURLException
	{
		FeedItem feedItem = new FeedItem();
		feedItem.setAuthor(new FeedItem.Author("test", "test@test"));
		feedItem.setDescription("test description");
		feedItem.setGuid(UUID.randomUUID().toString());
		feedItem.setLastUpdated(new Date());
		feedItem.setPublishDate(new Date());
		feedItem.setTitle("test title");
		feedItem.setUrl("http://test.test/test.rss");
		feedItem.setFeedId("515886fc9386178d1f386782");
		
		compareItemToDBObject(DataConverter.itemToData(feedItem), feedItem);
	}
	
	private void compareItemToDBObject(DBObject dbobject, FeedItem feedItem)
	{
		assertEquals((Date) dbobject.get("pubdate"), feedItem.getPublishDate());
		assertEquals((Date) dbobject.get("last_updated"), feedItem.getLastUpdated());
		assertEquals(((DBObject) dbobject.get("author")).get("name"), feedItem.getAuthor().getName());
		assertEquals(((DBObject) dbobject.get("author")).get("email"), feedItem.getAuthor().getEmail());
		assertEquals(dbobject.get("description"), feedItem.getDescription());
		assertEquals(dbobject.get("guid"), feedItem.getGuid());
		assertEquals(dbobject.get("feed_id"), feedItem.getFeedId());
		assertEquals(dbobject.get("title"), feedItem.getTitle());
		assertEquals(dbobject.get("url"), feedItem.getUrl().toExternalForm());
	}

}
